public class Factorial {

    static private long calculateFactorial(long input){
        long result = 1;
        for (; input > 1; input--) {
            result *= input;
        }
        return result;
    }
    public static void main( String[] args ) {
        long factorial = 20;
        try {
            if (args.length > 0) {
                System.out.println("factorial =  "+ calculateFactorial(Long.parseLong(args[0])));
            } else {
                System.out.println("factorial =  "+ calculateFactorial(factorial));
            }
        } catch (NumberFormatException exception) {
            exception.printStackTrace();
            System.out.println("================================");
            System.out.println("please dont enter invalid number");
            System.out.println("================================");
        }
    }

}